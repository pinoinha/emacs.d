;;; init.el --- Initialization file for Emacs
;;; Commentary: Emacs Startup File --- initialization for Emacs

;;; Code:

;; mudando o socket padrão
(setq server-socket-dir "/run/user/1000/emacs")

;; modo para arquivos do kmonad
(use-package kbd-mode
  :load-path "~/.emacs.d/elisp/")

;; modo para arquivos sxhkd
(use-package sxhkd-mode
  :load-path "~/.emacs.d/elisp/")

;; pacotes para o emacs
(require 'package)
(add-to-list 'package-archives
             '("melpa" . "https://melpa.org/packages/") t)

;; use-package, por facilidade
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))
(require 'use-package)

;; defvars pra evitar problemas
(defvar lsp-eldoc-hook)
(defvar ido-enable-flex-matching)
(defvar ido-everywhere)
(defvar ibuffer-saved-filter-groups)

;; rustic
(use-package rustic
  :ensure
  :bind (:map rustic-mode-map
              ("M-j" . lsp-ui-imenu)
              ("M-?" . lsp-find-references)
              ("C-c C-c l" . flycheck-list-errors)
              ("C-c C-c a" . lsp-execute-code-action)
              ("C-c C-c r" . lsp-rename)
              ("C-c C-c q" . lsp-workspace-restart)
              ("C-c C-c Q" . lsp-workspace-shutdown)
              ("C-c C-c s" . lsp-rust-analyzer-status))
  :config
  ;; uncomment for less flashiness
  (setq lsp-eldoc-hook nil)
  ;; (setq lsp-enable-symbol-highlighting nil)
  ;; (setq lsp-signature-auto-activate nil)

  ;; comment to disable rustfmt on save
  (setq rustic-format-on-save t)
  (add-hook 'rustic-mode-hook 'rk/rustic-mode-hook))

(defun rk/rustic-mode-hook ()
  ;; C-c C-c C-r works without having to confirm
  (when buffer-file-name
    (setq-local buffer-save-without-query t)))

;; flycheck
(use-package flycheck
  :ensure
  :init (global-flycheck-mode))

;; so flycheck doesn't keep complaining about init.el
(setq-default flycheck-disabled-checker '(emacs-lisp-checkdoc))

;; ativa o cursor blink
(blink-cursor-mode 1)

;; ativa linhas nos arquivos
(line-number-mode 1)
(global-display-line-numbers-mode)

;; ativa o autopair para inserir parênteses, aspas, etc. em pares
(electric-pair-mode 1)

;; enter sempre indenta com base na linha anterior
(electric-indent-mode 1)

;; sem barra de scroll, menu ou de tarefas
(scroll-bar-mode -1)
(menu-bar-mode -1)
(tool-bar-mode -1)

;; desativa a barra de rolagem em frames novos
(defun my/disable-scroll-bars (frame)
  (modify-frame-parameters frame
                           '((vertical-scroll-bars . nil)
                             (horizontal-scroll-bars . nil))))
(add-hook 'after-make-frame-functions 'my/disable-scroll-bars)

;; coloca todos os arquivos de auto-save em "~/.emacs.d/auto-save-list"
(setq backup-directory-alist
      `(("." . ,(concat user-emacs-directory "auto-save-list"))))

(setq-default indent-tabs-mode nil)
(setq-default tab-width 2)
(setq-default tab-always-indent 'complete)

;; customização da barra de modo
(setq mode-line-format
      (list
       "%e"
       ;; mode-name
       "%m: "
       ;; nome do buffer atual
       "[%b], "
       ;; número da linha atual
       "linha %l "
       "-- usuário: "
       ;; nome do usuário
       (getenv "USER")))

;; helm
(global-set-key (kbd "M-x") #'helm-M-x)
(global-set-key (kbd "C-x r b") #'helm-filtered-bookmarks)
(global-set-key (kbd "C-x C-f") #'helm-find-files)
(global-set-key (kbd "C-x b") #'helm-mini)
(helm-mode 1)

;; lsp-mode
(use-package lsp-mode
  :ensure
  :commands lsp
  :init
  ;; set prefix for lsp-command-keymap (few alternatives - "C-l", "C-c l")
  (setq lsp-keymap-prefix "s-l")
  :hook (
         (rust-mode . lsp)
         (c-mode . lsp)
         (python-mode . lsp)
         (java-mode . lsp)
         ;; if you want which-key integration
         (lsp-mode . lsp-enable-which-key-integration))
  :custom
  ;; what to use when checking on-save. "check" is default, I prefer clippy
  (lsp-rust-analyzer-cargo-watch-command "clippy")
  (lsp-eldoc-render-all t)
  (lsp-idle-delay 2.0)
  ;; enable / disable the hints as you prefer:
  (lsp-rust-analyzer-server-display-inlay-hints t)
  (lsp-rust-analyzer-display-lifetime-elision-hints-enable "skip_trivial")
  (lsp-rust-analyzer-display-chaining-hints t)
  (lsp-rust-analyzer-display-lifetime-elision-hints-use-parameter-names nil)
  (lsp-rust-analyzer-display-closure-return-type-hints t)
  (lsp-rust-analyzer-display-parameter-hints nil)
  (lsp-rust-analyzer-display-reborrow-hints nil)
  :config
  (add-hook 'lsp-mode-hook 'lsp-ui-mode))

;; lsp-ui config
(use-package lsp-ui
  :ensure
  :commands lsp-ui-mode
  :custom
  (lsp-ui-peek-always-show nil)
  (lsp-ui-sideline-show-hover nil)
  (lsp-ui-doc-enable nil))

;; optional if you want which-key integration
(use-package which-key
    :config
    (which-key-mode))
    (setq which-key-idle-delay 0.3)

;; ccls
(use-package ccls
  :config (setq ccls-executable "/usr/bin/ccls")
  :hook ((c-mode c++-mode objc-mode cuda-mode) .
         (lambda () (require 'ccls) (lsp))))

;; company
(use-package company
  :ensure
  :custom
  (company-idle-delay 0.5) ;; how long to wait until popup
  :config
  (setq company-global-modes '(not org-mode))
  :bind
  (:map company-active-map
          ("C-n". company-select-next)
          ("C-p". company-select-previous)
          ("M-<". company-select-first)
          ("M->". company-select-last))
  (:map company-mode-map
     ("<tab>" . #'company-indent-or-complete-common)
     ("TAB". #'company-indent-or-complete-common)))

;; yasnippets
(require 'yasnippet)
(use-package yasnippet
  :ensure
  :config
  (yas-reload-all)
  (add-hook 'prog-mode-hook 'yas-minor-mode)
  (add-hook 'text-mode-hook 'yas-minor-mode))

(defun do-yas-expand ()
  (let ((call-other-command 'return-nil))
    (yas-expand)))

(defun company-yasnippet-or-completion ()
  (interactive)
  (or (do-yas-expand)
      (company-complete-common)))

(defun check-expansion ()
  (save-excursion
    (if (looking-at "\\_>") t
      (backward-char 1)
      (if (looking-at "\\.") t
    (backward-char 1)
    (if (looking-at "::") t nil)))))

;;; org-mode config
(require 'org)
(require 'ob)

(progn
  (setq org-src-fontify-natively t)
  (setq org-startup-folded nil)
  (setq org-return-follows-link t)
  (setq org-startup-truncated nil)
  (setq org-startup-with-inline-images t)
  (setq org-hide-emphasis-markers t))

(add-hook 'org-mode-hook
          'visual-line-mode
          (lambda ()
            (company-mode -1)))

;; make org mode allow eval of some langs
(org-babel-do-load-languages
 'org-babel-load-languages
 '((emacs-lisp . t)
   (clojure . t)
   (python . t)
   (ruby . t)
   (shell . t)))

;; sem mais confirmação para validar no babel
(setq org-confirm-babel-evaluate nil)

;; sintaxe colorida no babel
(setq org-src-fontify-natively t)

;; torna a seleção do estado da tarefa mais fácil
(setq org-todo-keywords
      '((sequence "TODO(t)" "|" "DONE(d)")
        (sequence "BUG(b)" "|" "FIXED(f)")
        (sequence "|" "CANCELED(c)")))

;; ibuffer
(require 'ibuf-ext)
(setq ibuffer-saved-filter-groups
      (quote (("default"
               ("dired" (mode . dired-mode))
               ("perl" (mode . cperl-mode))
               ("prog" (or
                        (mode . rustic-mode)
                        (mode . python-mode)
                        (mode . c-mode)
                        (mode . cperl-mode)))
               ("erc" (mode . erc-mode))
               ("planner" (or
                           (name . "^\\*Calendar\\*$")
                           (name . "^diary$")
                           (mode . muse-mode)))
               ("emacs" (or
                         (name . "^\\*scratch\\*$")
                         (name . "^\\*Messages\\*$")
                         (mode . emacs-lisp-mode)
                         (name . "\\.el")
                         (name . "^\\*eshell\\*$")))
               ("org" (mode . org-mode))
               ("elfeed" (name . "^\\*elfeed\\*$"))
               ("helm" (mode . helm-major-mode))))))

(add-hook 'ibuffer-mode-hook
              (lambda ()
                (ibuffer-switch-to-saved-filter-groups "default")))

(global-set-key (kbd "C-x C-b") 'ibuffer)

;; winum
(require 'winum)
(winum-mode)

;; dired
(require 'ls-lisp)
(setq ls-lisp-use-insert-directory-program nil)

;; treemacs
(require 'treemacs)
(global-set-key (kbd "M-o") 'treemacs)

(use-package treemacs-icons-dired
  :hook (dired-mode . treemacs-icons-dired-enable-once)
  :ensure t)

(use-package treemacs-magit
  :after (treemacs magit)
  :ensure t)

(use-package treemacs-tab-bar ;;treemacs-tab-bar if you use tab-bar-mode
  :after (treemacs)
  :ensure t
  :config (treemacs-set-scope-type 'Tabs))

;; elfeed
(use-package elfeed
  :ensure t
  :config
  (setq elfeed-curl-program-name "/usr/bin/curl")
  (setq elfeed-use-curl t)
  (setq elfeed-db-directory "~/.emacs.d/elfeed")
  (setq elfeed-feeds
        '(("https://www.reddit.com/r/unixporn/new.rss" reddit)
          ("https://www.reddit.com/r/UsabilityPorn/new.rss" reddit)
          ("https://www.reddit.com/r/jovemnerd/new.rss reddit" reddit)
          ("https://www.reddit.com/r/betterunixporn/new.rss")
          ("https://www.reddit.com/r/suckless/new.rss" reddit)
          ("https://www.reddit.com/r/irc/new.rss" reddit)
          ("https://www.reddit.com/r/tokipona/new.rss" reddit)
          ("https://www.reddit.com/r/unixporn/new.rss" reddit)
          ("https://www.reddit.com/r/UsabilityPorn/new.rss" reddit)
          ("https://www.reddit.com/r/jovemnerd/new.rss reddit" reddit)
          ("https://www.reddit.com/r/betterunixporn/new.rss")
          ("https://www.reddit.com/r/suckless/new.rss" reddit)
          ("https://www.reddit.com/r/irc/new.rss" reddit)
          ("https://www.reddit.com/r/tokipona/new.rss" reddit)
          ("https://voidlinux.org/atom.xml" linux news)
          ("https://archlinux.org/feeds/news/" linux news)
          ("https://artixlinux.org/feed.php" linux news)
          ("https://www.gentoo.org/feeds/news.xml" linux news)
          ("https://www.debian.org/News/weekly/dwn.en.rdf" linux news)
          ("https://www.undeadly.org/cgi?action=rss" bsd news)
          ("https://www.youtube.com/feeds/videos.xml?channel_id=UCld68syR8Wi-GY_n4CaoJGA" youtube linux)
          ("https://www.youtube.com/feeds/videos.xml?channel_id=UC7YOGHUfC1Tb6E4pudI9STA" youtube linux)
          ("https://feeds.soundcloud.com/users/soundcloud:users:33700067/sounds.rss" podcast)
          ("https://feeds.feedburner.com/XadrezVerbal" podcast)
          ("https://manualdousuario.net/feed/podcast/tecnocracia" podcast)
          ("http://rss.acast.com/themagnusarchives" podcast)
          ("https://www.digitalpodcast.com/feeds/go_rss?feed_id=92611" podcast)
          ("http://feeds.nightvalepresents.com/welcometonightvalepodcast" podcast)
          ("https://pinoinha.bearblog.dev/feed/" blog)
          ("https://wiki.caninosloucos.org/api.php?hidebots=1&urlversion=1&days=7&limit=50&action=feedrecentchanges&feedformat=atom" blog wiki)
          ("https://buttondown.email/fractalverse/rss" blog)
          ("https://protesilaos.com/codelog.xml" blog)
          ("https://suckless.org/atom.xml" news)
          ("https://xkcd.com/atom.xml" tirinha)
          ("https://www.smbc-comics.com/comic/rss" tirinha)
          ("https://drewdevault.com/blog/index.xml" blog linux)
          ("https://blackgnu.net/atom.xml" blog linux)
          ("https://porcellis.com/index.xml" blog linux)
          ("https://dataswamp.org/solene/rss.xml" blog bsd)
          ("https://unixsheikh.com/feed.rss" blog unix linux bsd)
          ("https://based.cooking/rss.xml" receita)
          ("https://www.romanzolotarev.com/rss.xml" blog bsd)
          ("https://ariadnavigo.xyz/posts/index.xml" blog linux)
          ("https://landchad.net/rss.xml" tutorial linux)
          ("http://rachelbythebay.com/w/atom.xml" blog)
          ("https://baixacultura.org/feed" blog)
          ("https://webzine.puffy.cafe/atom.xml" zine bsd)
          ("https://alicef.me/feeds/all.atom.xml" blog)
          ("https://codemadness.org/rss_content.xml" blog dev)
          ("https://www.uninformativ.de/blog/feeds/en.atom" blog)
          ("https://computer.rip/rss.xml" blog)
          ("https://ariadne.space/feed/" blog linux))))

(add-hook 'elfeed-new-entry-hook
          (elfeed-make-tagger :feed-url "youtube\\.com"
                              :add '(youtube)))

;; usar o eww ao invés de navegadores externos
(setq browse-url-browser-function 'eww-browse-url)

;; emms 
(require 'emms-setup)
(emms-all)
(emms-default-players)

;; estética

;; bg/fg geral
(setq default-frame-alist
      '((background-color . "#101112")
        (foreground-color . "#f5f5f5")
        (font . "Iosevka 14")))

;; modeline
(set-face-foreground 'mode-line "#e60053")
(set-face-background 'mode-line "#272829")
(set-face-background 'mode-line-inactive "dark gray")
(set-face-attribute 'mode-line nil
                    :box '(:width 0))

;; transparênca
(set-frame-parameter (selected-frame) 'alpha '(85 . 60))
(add-to-list 'default-frame-alist
         '(alpha . (85 . 60))
         '(vertical-scroll-bars . nil))

;; configs globais
(add-hook 'after-init-hook 'global-company-mode)

;; python venvs
(use-package auto-virtualenv
  :ensure t
  :init
  (use-package pyvenv
    :ensure t)
  :config
  (add-hook 'python-mode-hook 'auto-virtualenv-set-virtualenv)
)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(notmuch-saved-searches
   '((:name "inbox" :query "tag:inbox" :key "i" :sort-order newest-first :search-type tree)
     (:name "unread" :query "tag:unread" :key "u" :sort-order newest-first)
     (:name "flagged" :query "tag:flagged" :key "f" :sort-order newest-first)
     (:name "drafts" :query "tag:draft" :key "d" :sort-order newest-first)
     (:name "all mail" :query "*" :key "a" :sort-order newest-first)
     (:name "sent" :query "from:pinoinha@disroot.org or from:matheusrbarros03@gmail.com or from:matheusrbarros@usp.br")))
 '(package-selected-packages
   '(racket-mode auto-virtualenv ssh-agency emms org-present flycheck-checkbashisms helm-lsp emojify babel elfeed winum yasnippet-snippets yasnippet company rustic helm ccls magit toml-mode lsp-java notmuch markdown-mode use-package flycheck-pycheckers flycheck-haskell flycheck)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
